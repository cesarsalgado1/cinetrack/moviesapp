import { Component, OnInit } from '@angular/core';
import { TrackTvService } from './service/tracktv.service';
import { CineTrackService } from './service/cinetrack.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],  
})
export class AppComponent implements OnInit {  
  // API TRACK.TV
  tbtrending: any[] = [];  
  trendingItemCount: number = 0;

  tbpopular: any[] = [];  
  popularItemCount: number = 0;

  // API CINE TRACK :TredingMovieService
  tbtTrending: any[] = [];
  cinetrendingItemCount: number = 0; 
  messageSaveTrending:string = "";
  messageGetTrending:string = "";

  // API CINE TRACK :PopularMovieService
  tbtPopular: any[] = [];
  cinepopularItemCount: number = 0; 
  messageSavePopular:string = "";
  messageGetPopular:string = "";  
  progressValue: number = 0;
  isLoading: boolean = false;
  totalToFetch: number = 0;

  // API CINE TRACK : SearchMovieService
  tbtSearch: any[] = [];
  movieTitle: string = '';

  // API CINE TRACK : HomeMovieService
  tbtRandom: any[] = [];

  constructor(
    private tracktvService: TrackTvService,
    private cineTrackService : CineTrackService,
    private service: MessageService
  ) { }

  ngOnInit(): void {
    
  }

  //#region  APIS TRACK TV  
  async getTrendingMovies(limit : number = 10){
    const params = { page: 1, limit: limit }    
    try {
      const response : any = await this.tracktvService.getTrendingMovies(params).toPromise();
      this.tbtrending = response.movies;      
      this.trendingItemCount = response.itemCount   
      this.showSuccess("Get trending movies from the API Track.tv");
      localStorage.setItem('trendingItemCount', response.itemCount.toString()); 
    } catch (error) {
      this.showError("Error fetching trending movies");
      console.error('Error fetching trending movies', error);      
    }  
  }

  async getPopularMovies(){
    const params = { page: 1, limit: 10 }  
    try {
      const response : any = await this.tracktvService.getPopularMovies(params).toPromise();
      this.tbpopular = response.movies;      
      this.popularItemCount = response.itemCount   
      this.showSuccess("Get popular movies from the API Track.tv");
      localStorage.setItem('popularItemCount', response.itemCount.toString()); 
    } catch (error) {
      console.error('Error fetching popular movies', error);
      this.showError("Error fetching popular movies");
    }    
  }
  //#endregion
  
  //#region API CINE TRACK: TrendingMovieService
  async saveTrendingMovies_CineTrack() {
    const itemCount = parseInt(localStorage.getItem('trendingItemCount') || '0', 10) + 1000
    const params = { page: 1, limit: itemCount };
  
    try {
      const trends: any = await this.tracktvService.getTrendingMovies(params).toPromise();
      const listTrending: any = trends.movies
            
      const response = await this.cineTrackService.saveTrendingMovies(listTrending).toPromise();   
      if (response.success){
        this.showSuccess(response.message)
        this.cinetrendingItemCount = trends.itemCount
        this.messageSaveTrending = response.message        
        localStorage.removeItem('trendingItemCount')
      }      
    } catch (error) {
      console.error('Error fetching or saving trending movies', error);
      this.showError("Error fetching or saving trending movies")
    }    
  }

  async getTrendingMovies_CineTrack(){
    const response = await this.cineTrackService.getTrendingMovies().toPromise();  
    if (response.success){
      this.tbtTrending = response.data
      this.showSuccess(response.message)                  
      this.messageGetTrending = response.message            
    }else{
      this.showWarn(response.message)
    }
  }

  //#endregion

  //#region API CINE TRACK: PopuparMovieService
  async savePopularMovies_CineTrack(){
    const batchSize = 1000; // Tamaño del lote que se obtendrá y procesará en cada iteración
    let page = 1; // Página inicial
    let totalFetched = 0; // Total de registros obtenidos
    this.totalToFetch = parseInt(localStorage.getItem('popularItemCount') || '0', 10) + 1000;
    this.progressValue = 0;
    this.isLoading = true;
    let popularItemCount : number = 0;

    try {
      while (totalFetched < this.totalToFetch) {
        const params = { page: page, limit: batchSize };
        
        // Obtener un lote de películas populares
        const popular: any = await this.tracktvService.getPopularMovies(params).toPromise();
        const listPopular: any = popular.movies;
        popularItemCount = popular.itemCount
  
        if (listPopular && listPopular.length > 0) {
          // Guardar el lote de películas populares en la base de datos
          const response = await this.cineTrackService.savePopularMovies(listPopular).toPromise();
  
          if (response.success){
            totalFetched += listPopular.length; // Actualizar el total de registros obtenidos
            this.progressValue = parseFloat(((totalFetched / this.totalToFetch) * 100).toFixed(2)); // Actualizar la barra de progreso con dos decimales
            page++; // Incrementar la página para la siguiente iteración
          } else {
            throw new Error("Error saving popular movies batch");
          }
        } else {
          break; // Salir del bucle si no hay más registros para obtener
        }
      }
  
      // Actualizar los valores necesarios después de completar la obtención y almacenamiento      
      this.isLoading = false;      
      this.showSuccess("Successfully fetched and saved popular movies");
      this.messageSavePopular = "Successfully fetched and saved popular movies";
      localStorage.removeItem('popularItemCount');
      this.cinepopularItemCount = popularItemCount
    } catch (error) {
      console.error('Error fetching or saving popular movies', error);
      this.showError("Error fetching or saving popular movies");
    }
  }
  
  async getPopularMovies_CineTrack(){
    const response = await this.cineTrackService.getPopularMovies().toPromise();  
    if (response.success){
      this.tbtPopular = response.data
      this.showSuccess(response.message)                  
      this.messageGetPopular = response.message            
    }else{
      this.showWarn(response.message)
    }
  }

  //#endregion

  //#region API CINE TRACK: SearchMovieService
  async searchTrendingAndPopularMovies(movieTitle: string){    
    const response = await this.cineTrackService.searchTrendingAndPopularMovies(movieTitle).toPromise();  
    if (response.success){
      this.tbtSearch = response.data      
      this.showSuccess(response.message)                        
    }else{
      this.showWarn(response.message)
    }
  }
  //#endregion

  //#region API CINE TRACK: HomeMovieService
  async getRandomMovies(){
    const response = await this.cineTrackService.getRandomMovies().toPromise();  
    if (response.success){
      this.tbtRandom = response.data      
      this.showSuccess(response.message)                        
    }else{
      this.showWarn(response.message)
    }
  }
  //#endregion 

  //#region Mensajes
  showSuccess(message : any) {
    this.service.add({ key: 'tst', severity: 'success', summary: 'Success Message', detail: message });
  }
  showWarn(message : any) {
    this.service.add({ key: 'tst', severity: 'warn', summary: 'Warn Message', detail: message });
  }
  showError(message : any) {
    this.service.add({ key: 'tst', severity: 'error', summary: 'Error Message', detail: message });
  }
  //#endregion

}
