import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TrackTvService {
  public headTrackTv!: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headTrackTv = new HttpHeaders({
      'Content-Type': 'application/json',
      'trakt-api-version': environment.TRAKT_API_VERSION,
      'trakt-api-key': environment.TRAKT_API_KEY,
    });
  }

  getTrendingMovies(params: any) {
    return this.http.get(environment.API_TRACKTV_TRENDING_URL, {headers: this.headTrackTv,params,observe: 'response',})
      .pipe(map((response) => {        
        return {
          movies: response.body,
          pageCount: response.headers.get('x-pagination-page-count'),
          itemCount: response.headers.get('x-pagination-item-count'),
        };
        })
      );
  }

  getPopularMovies(params: any) {
    return this.http.get(environment.API_TRACKTV_POPULAR_URL, {headers: this.headTrackTv,params,observe: 'response',})
      .pipe(map((response) => {          
          return {
            movies: response.body,
            pageCount: response.headers.get('x-pagination-page-count'),
            itemCount: response.headers.get('x-pagination-item-count'),
          };
        })
      );
  }

}
