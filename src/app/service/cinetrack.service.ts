import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CineTrackService {
  private apiTrending = 'api/Trending/';
  private apiPopular = 'api/Popular/';
  private apiSearch = 'api/Search/';
  private apiHome = 'api/Home/';

  constructor(private http: HttpClient) {}

  saveTrendingMovies(listTrending: any) {
    return this.http.post<any>(environment.trendingApiUrl + this.apiTrending + 'SaveTrendingMovies', listTrending).pipe(map((data) => data));
  }

  getTrendingMovies() {
    return this.http.get<any>(environment.trendingApiUrl + this.apiTrending + 'GetTrendingMovies').pipe(map((data) => data));
  }

  savePopularMovies(popularData: any) {
    return this.http.post<any>(environment.popularApiUrl + this.apiPopular + 'SavePopularMovies', popularData).pipe(map((data) => data));
  }

  getPopularMovies() {
    return this.http.get<any>(environment.popularApiUrl + this.apiPopular + 'GetPopularMovies').pipe(map((data) => data));
  }

  searchTrendingAndPopularMovies(searchTerm: string){
    return this.http.get<any>(environment.searchApiUrl + this.apiSearch + 'SearchMovies?searchTerm='+ searchTerm).pipe(map((data) => data));
  }

  getRandomMovies(){
    return this.http.get<any>(environment.homeApiUrl + this.apiHome + 'GetRandomMovies').pipe(map((data) => data));
  }


}
