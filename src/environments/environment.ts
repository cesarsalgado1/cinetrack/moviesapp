// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  trendingApiUrl: "https://localhost:7179/",
  popularApiUrl: "https://localhost:7030/",
  searchApiUrl: "https://localhost:7093/",
  homeApiUrl: "https://localhost:7130/",

  API_TRACKTV_TRENDING_URL: 'https://api.trakt.tv/movies/trending',
  API_TRACKTV_POPULAR_URL: 'https://api.trakt.tv/movies/popular',
  TRAKT_API_KEY: '21f0de12f95eba39ab4134d53d7c46916fae1d396f4450b6f0dfbe322f5e6867',
  TRAKT_API_VERSION: '2'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
